import { Module } from '@nestjs/common';

@Module({})
export class AppModule {
  async beforeApplicationShutdown() {
    console.log('destroying...')
    await new Promise((resolve) => setTimeout(resolve, 1000*3))
  }
}
